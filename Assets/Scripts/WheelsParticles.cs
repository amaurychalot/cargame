using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelsParticles : MonoBehaviour
{
    public Transform DriftTrailPrefab;
    public static Transform DriftTrailDetachedParent;
    //public ParticleSystem driftParticles;
    
    private Transform m_DriftTrail;
    private WheelCollider m_WheelCollider;

    public bool Drifting = false;

    private void Start()
    {
        /*driftParticles = transform.root.GetComponent<ParticleSystem>();

        if (driftParticles == null)
        {
            Debug.LogWarning(" no particle system found on car to generate smoke particles", gameObject);
        }
        else
        {
            driftParticles.Stop();
        }*/
        
        m_WheelCollider = GetComponent<WheelCollider>();
        
        if (DriftTrailDetachedParent == null)
        {
            DriftTrailDetachedParent = new GameObject("Skid Trails - Detached").transform;
        }
    }

    public void EmitDrifting()
    {

        if (!Drifting)
        {
            StartCoroutine(StartDriftingTrail());
        }
    }
    
    public IEnumerator StartDriftingTrail()
    {
        Drifting = true;
        m_DriftTrail = Instantiate(DriftTrailPrefab);

        while (m_DriftTrail == null)
        {
            yield return null;
        }

        m_DriftTrail.parent = transform;
        m_DriftTrail.localPosition = -Vector3.up*m_WheelCollider.radius;
    }

    public void EndDriftingTrail()
    {
        if (!Drifting)
        {
            return;
        }

        Drifting = false;
        m_DriftTrail.parent = DriftTrailDetachedParent;
        Destroy(m_DriftTrail.gameObject, 10);
    }
}
